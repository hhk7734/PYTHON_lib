from setuptools import setup, find_packages

setup(
    name='armss',
    version='',
    packages=find_packages(exclude=['docs', 'tests']),
    url='',
    license='',
    author='hong',
    author_email='',
    description='',
    install_requires=['python3-xlib', 'pyserial']
)
