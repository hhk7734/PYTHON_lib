from Xlib import X
from Xlib.ext.xtest import fake_input
from Xlib.XK import string_to_keysym
import uinput
import time


class MouseController:
    BUTTON_EVENT = {'LEFT': 1, 'RIGHT': 2, 'WHEEL': 3, 'WHEEL_UP': 4, 'WHEEL_DOWN': 5}

    def __init__(self, display):
        self.__display = display
        self.__screen = self.__display.screen()
        self.__root = self.__screen.root

        root_point = self.__screen.root.query_pointer()
        self.__mouse_x = root_point.root_x
        self.__mouse_y = root_point.root_y
        self.__screen_size = {'x': self.__screen.width_in_pixels, 'y': self.__screen.width_in_pixels}

    def plus_mouse_x(self, x: int, multiple=1):
        if x == 1 or x == -1:
            return
        self.__mouse_x += (x * multiple)
        if self.__mouse_x > self.__screen_size['x']:
            self.__mouse_x = self.__screen_size['x']
        elif self.__mouse_x < 0:
            self.__mouse_x = 0

    def plus_mouse_y(self, y: int, multiple=1):
        if y == 1 or y == -1:
            return
        self.__mouse_y += (y * multiple)
        if self.__mouse_y > self.__screen_size['y']:
            self.__mouse_y = self.__screen_size['y']
        elif self.__mouse_y < 0:
            self.__mouse_y = 0

    def __mouse_warp(self):
        self.__root.warp_pointer(self.__mouse_x, self.__mouse_y)
        self.__display.sync()

    def _mouse_press(self, what):
        fake_input(self.__display, X.ButtonPress, what)
        self.__display.sync()

    def _mouse_release(self, what):
        fake_input(self.__display, X.ButtonRelease, what)
        self.__display.sync()

    def mouse_click(self, button):
        self._mouse_press(button)
        self._mouse_release(button)

    def relative_mouse_move(self, x, y, intensity=2):
        self.plus_mouse_x(x, intensity)
        self.plus_mouse_y(y, intensity)
        self.__mouse_warp()

    def absolute_mouse_move(self, x, y, intensity=2):
        x = x * intensity
        y = y * intensity
        self.__display.warp_pointer(x, y)
        self.__display.sync()


class NativeDeviceController:

    def __init__(self):
        self.__event = [uinput.REL_X, uinput.REL_Y, uinput.BTN_LEFT, uinput.BTN_RIGHT, uinput.KEY_0,
                        uinput.KEY_W, uinput.KEY_A, uinput.KEY_S, uinput.KEY_D, uinput.KEY_SPACE, uinput.KEY_LEFTSHIFT,
                        uinput.KEY_SPACE, uinput.KEY_R, uinput.KEY_LEFTCTRL, uinput.KEY_E]
        self.__KEY_CODE = {'w': uinput.KEY_W, 'a': uinput.KEY_A, 's': uinput.KEY_S, 'd': uinput.KEY_D,
                           'shift': uinput.KEY_LEFTSHIFT, 'space': uinput.KEY_SPACE, '0': uinput.KEY_0,
                           'r': uinput.KEY_R, 'ctrl': uinput.KEY_LEFTCTRL, 'e': uinput.KEY_E}

        self.__device = uinput.Device(self.__event)

    def __del__(self):
        self.__device.destroy()

    def umount_device(self):
        self.__device.destroy()

    @property
    def event(self):
        return self.__event

    @event.setter
    def event(self, value):
        self.__event.append(value)

    def mouse_move(self, x, y, intensity=2):
        if uinput.REL_X and uinput.REL_Y in self.__event:
            x = x // 50 * intensity
            y = y // 50 * intensity
            self.__device.emit(uinput.REL_X, x, syn=False)
            self.__device.emit(uinput.REL_Y, y)

    def mouse_click(self, button):
        self.__device.emit_click(button)

    def key_click(self, key):
        self.__device.emit(self.__KEY_CODE[key], 1)
        time.sleep(0.01)
        self.__device.emit(self.__KEY_CODE[key], 0)

    def key_press(self, key):
        self.__device.emit(self.__KEY_CODE[key], 1)

    def key_release(self, key):
        self.__device.emit(self.__KEY_CODE[key], 0)


class KeyboardController:

    def __init__(self, display):
        self.__display = display
        self.key_pressing = False

    def key_press(self, key):
        key = self.__display.keysym_to_keycode(string_to_keysym(key))
        fake_input(self.__display, X.KeyPress, key)
        self.__display.sync()

    def key_release(self, key):
        key = self.__display.keysym_to_keycode(string_to_keysym(key))
        fake_input(self.__display, X.KeyRelease, key)
        self.__display.sync()

    def key_click(self, key):
        self.key_press(key)
        self.key_release(key)
