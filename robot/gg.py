from serialMove import MouseController, KeyboardController, NativeDeviceController
from Xlib import display
import serial
import numpy as np

rotx = np.array([[1.0, 0.0, 0.0, 0.0],
                 [0.0, 0.0, -1.0, 0.0],
                 [0.0, 1.0, 0.0, 0.0],
                 [0.0, 0.0, 0.0, 1.0]])

aim_X = -0.4
aim_Y = -0.4
aim_Z = 1.5


def DH(angle, d, l):
    global rotx
    trans = np.eye(4)
    trans[0, 3] = l
    trans[2, 3] = d
    rotz = np.array([[np.cos(angle), -np.sin(angle), 0., 0.],
                     [np.sin(angle), np.cos(angle), 0., 0.],
                     [0., 0., 1., 0.],
                     [0., 0., 0., 1.]])
    rotz = np.dot(rotz, trans)
    return np.dot(rotz, rotx)


def run():
    global aim_X, aim_Y, aim_Z
    d = display.Display()
    m = MouseController(d)
    k = KeyboardController(d)
    try:
        s = serial.Serial('/dev/ttyUSB0', 115200)
    except Exception as e:
        print(str(e))
        exit()
    s.readline()
    n = NativeDeviceController()
    press_key = {'w': False, 'a': False, 's': False, 'd': False,
                 'space': False, 'shift': False, 'ctrl': False, 'e': 0}
    while True:
        try:
            a = s.readline().decode('utf-8').strip().split(',')
            arm = list(map(float, a[:7]))
            arm = np.array(arm)/1000.0
            endpoint = np.dot(DH(0.0, 1.7, 0.0), DH(arm[1], 0.2, 0.0))
            endpoint = np.dot(endpoint, DH(arm[2], 0.0, 0.0))
            endpoint = np.dot(endpoint, DH(arm[3], 0.3, 0.0))
            endpoint = np.dot(endpoint, DH(arm[4], 0.0, 0.0))
            endpoint = np.dot(endpoint, DH(arm[5], 0.3, 0.0))
            finger = list(map(int, a[7:]))
        except Exception:
            continue
        if len(finger) != 5:
            continue

        # print(arm)
        arm_X = endpoint[0, 3]
        arm_Y = endpoint[1, 3]
        arm_Z = endpoint[2, 3]
        # print("{0:.2f}, {1:.2f}, {2:.2f}".format(
        #     endpoint[0, 3], endpoint[1, 3], endpoint[2, 3]))
# finger
        if finger[0] > 30:
            n.key_click('ctrl')
        if finger[1] > 30:
            m._mouse_press(1)
        else:
            m._mouse_release(1)
        if finger[2] < 30 and finger[3] > 30 and finger[4] > 30 and not press_key['w']:
            n.key_press('w')
            press_key['w'] = True
        elif finger[2] > 30 and press_key['w']:
            n.key_release('w')
            press_key['w'] = False
        if finger[2] < 30 and finger[3] < 30 and finger[4] < 30 and not press_key['s']:
            n.key_press('s')
            press_key['s'] = True
        elif finger[2] > 30 and press_key['s']:
            n.key_release('s')
            press_key['s'] = False
        if finger[2] > 30 and finger[3] > 30 and finger[4] < 30:
            n.key_click('r')
        if finger[2] < 30 and finger[3] > 30 and finger[4] < 30 and press_key['e'] > 100:
            n.key_click('e')
            press_key['e'] = 0
        elif press_key['e'] < 105:
            press_key['e'] += 1

        if finger[0] > 30 and finger[1] > 30 and finger[2] > 30 and finger[3] > 30 and finger[4] > 30:
            aim_X = arm_X
            aim_Y = arm_Y
            aim_Z = arm_Z
            print("aim")

        # left
        if arm_Y < aim_Y - 0.03:
            m.relative_mouse_move(-3, 0, 3)
            print("left")
        # right
        if arm_Y > aim_Y + 0.03:
            m.relative_mouse_move(3, 0, 3)
            print("right")
        # down
        if arm_Z < aim_Z - 0.03:
            m.relative_mouse_move(0, 3, 3)
            print("down")
        # up
        if arm_Z > aim_Z + 0.03:
            m.relative_mouse_move(0, -3, 3)
            print("up")

        print("{0:.2f} , {1:.2f} , {2:.2f}".format(
            arm_X-aim_X, arm_Y-aim_Y, arm_Z-aim_Z))

# arm


run()
