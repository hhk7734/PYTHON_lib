import serial
import serial.tools.list_ports

ser = serial.Serial()
ser.baudrate = 115200
ser.timeout = 1.5 # seconds
ser.write_timeout = 1.5

# find connected port
connected = []
for element in serial.tools.list_ports.comports():
    connected.append(element.device)
for i in range(0,len(connected)):
    print("{0} : {1}".format(i,connected[i]))
port_num=input("select port number : ")
ser.port = connected[int(port_num)]

# serial open
print("\nprot : {0}".format(ser.port))
print("baudrate : {0}".format(ser.baudrate))
try:
    ser.open()
except Exception as e:
    print(str(e))
    exit()
print("start")

# serial read
try:
    while True:
        ser_receive = ser.readline()
        if ser_receive == b'':
            continue

        print(ser_receive.decode('utf-8'))
except KeyboardInterrupt: # <C-C> stop
    print("\nserial close")
    ser.flush()
    ser.close()
    exit()
